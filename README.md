# 1km

Fork du projet [100km](https://framagit.org/Sp3r4z/100km), sous licence AGPL 

## À quoi ça sert ?
Tracer un cercle de 1km de rayon autour d'un point donné

## Limite connue
- Ça ne prend pas en compte les règles locales (plages…)

## Possibilités
- Faire une recherche avec le champ de recherche
- Avoir une zone de 1km au *clic*
- Faire des unions de plusieurs zones (zones communes)
- Se géolocaliser (nécessite HTTPS)

## Technologies
- [OpenStreetMap](https://www.openstreetmap.org/)
- [Nominatim](https://nominatim.openstreetmap.org/)
- [Leaflet](https://leafletjs.com/)
- [Leaflet Control Geocoder](https://github.com/perliedman/leaflet-control-geocoder)
- [Turf](https://turfjs.org/)
