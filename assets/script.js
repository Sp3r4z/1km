const createCircle = (lat,lng) => {
  let circle = L.circle([lat, lng], {radius: 1000})
  return L.layerGroup([circle])
},
createMarker = (lat,lng) => {
  let marker = L.marker([lat, lng])
  return marker
},
createPoint = (lat,lng) => {
  let marker = createMarker(lat,lng),
  circle = createCircle(lat,lng),
  layer = null

  layer = L.layerGroup([marker,circle])
  layersArray.push(layer)
  addToLayerGroup(layer)
},
addToLayerGroup = (elm) => {
  layerGroup.addLayer(elm)
},
removeLastPoint = _ => {
  if(layersArray.length > 0) {
    let layer = layersArray.pop()
    layerGroup.removeLayer(layer)
  }
},
clearLayer = _ => {
  layerGroup.clearLayers()
}

let theMap = L.map('map').setView([46.415,1.461], 6),
layersArray = [],
layerGroup = L.layerGroup()
layerGroup.addTo(theMap)

L.tileLayer('https://{s}.tile.osm.org/{z}/{x}/{y}.png', {
  attribution: '© <a href="https://www.openstreetmap.org/copyright">OpenStreetMap contributors</a>',
  maxZoom: 19,
}).addTo(theMap)

L.Control.geocoder({
  defaultMarkGeocode: false,
  geocoder: new L.Control.Geocoder.Nominatim({
    geocodingQueryParams: {
      'accept-language': 'fr',
      countrycodes: 'fr'
    },
    reverseQueryParams: {
      'accept-language': 'fr'
    }
  })
})
.on('markgeocode', (e) => {
  let center = e.geocode.center

  createPoint(center.lat,center.lng)
})
.addTo(theMap)

theMap.on('click', (e) => {
  let lat = e.latlng.lat,
  lon = e.latlng.lng

  createPoint(lat,lon)
})

const customControlClean =  L.Control.extend({
  options: {
    position: 'topleft'
  },

  onAdd: function () {
    let clearButton = L.DomUtil.create('button')
    clearButton.innerHTML = "Réinitialiser la carte"
    clearButton.onclick = ev => {
      L.DomEvent.stopPropagation(ev);
      clearLayer()
    }

    return clearButton;
  }
}),
customControlPoint = L.Control.extend({
  options: {
    position: 'topleft'
  },

  onAdd: function () {
    let removeLastLayer = L.DomUtil.create('button')
    removeLastLayer.innerHTML = "Défaire le dernier point"
    removeLastLayer.onclick = ev => {
      L.DomEvent.stopPropagation(ev);
      removeLastPoint()
    }

    return removeLastLayer;
  }
}),
customControlLocateMe = L.Control.extend({
  options: {
    position: 'topright'
  },

  onAdd: function () {
    let locateMe = L.DomUtil.create('button')
    locateMe.innerHTML = "Localise moi"
    locateMe.onclick = ev => {
      L.DomEvent.stopPropagation(ev);

      navigator.geolocation.getCurrentPosition(position => {
        createPoint(position.coords.latitude,position.coords.longitude)
      })
    }
    return locateMe;
  }
})

theMap.addControl(new customControlPoint());
theMap.addControl(new customControlClean());

// navigator.geolocation needs HTTPS to works
if ( location.protocol == 'https:' ) {
  theMap.addControl(new customControlLocateMe());
}
